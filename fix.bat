@echo off
if %1.==Sub. goto %2
mkdir bak 2>nul

for %%f in (*.avi) do call %0 Sub action %%f
goto end

:action
echo Converting %3
ffmpeg.exe -i %3 -c copy good_%3 2>nul
if ERRORLEVEL 1 goto:BAD
copy /y %3 bak/%3 >nul 2>nul
move good_%3 %3 >nul 2>&1
goto:GOOD
:GOOD
ECHO %3 converted ok
goto:END
:BAD
ECHO Error converting %3 
:END
